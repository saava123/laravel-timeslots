<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarbonController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/dates', [CarbonController::class, 'getDates']);
Route::post('/book', [CarbonController::class, 'book']);
Route::post('/slots', [CarbonController::class, 'doctorSlots']);
Route::post('/generateSlots', [CarbonController::class, 'runSlotsPerDay']);
Route::post('/allSlots', [CarbonController::class, 'allSlots']);
