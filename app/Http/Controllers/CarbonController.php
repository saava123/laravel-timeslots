<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use App\Models\Booking;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Models\DoctorCalendar;

class CarbonController extends Controller
{
    public function getDates(Request $request)
    {
        // dd($this->recurringDates());
        $period = CarbonPeriod::create($request->contractStartDate, $request->contractEndDate);

        $dates = $period->toArray();

        $checkDoctorIdExists = DoctorCalendar::where('doctor_id','=', $request->doctorId)->exists();

        // skip calendar creation if id already exists
        if(!$checkDoctorIdExists)
        {
            foreach($dates as $date)
            {
                $calendar = new DoctorCalendar();
                $calendar->day = $date->format('l');
                $calendar->date = $date->format('Y-m-d');
                $calendar->doctor_id=$request->doctorId;
                $calendar->save();
            }
        }

        $doctorExists = Booking::where('doctor_id', $request->doctorId)->exists();
        $getDoctorCalendar = DoctorCalendar::where('doctor_id', $request->doctorId)->get();

        // check if doctor already exists and avoid slots creation more tahn once
        if (!$doctorExists && $request->weeklyFormat === 1) {
            foreach ($getDoctorCalendar as $data) {
                // store slots for a weekly format
                    foreach ($request->weekly as $slot) {
                        $obj = (object)$slot;
                        // dd($obj);
                        if ($data->day === $obj->name) {
                            $booking = new Booking;
                            $booking->doctor_id = $request->doctorId;
                            $booking->name = $request->get('name');
                            $booking->date = $data->date;
                            $booking->slots = $this->getTimeSlots(20,$obj->start,$obj->end);
                            $booking->day = $data->day;
                            $booking->weekly_format = 1;
                            $booking->save();
                        }
                    }
            }
        }
        // elseif(!$doctorExists && $request->weeklyFormat === 2){
        //     foreach ($getDoctorCalendar as $data) {
        //         // store slots for a weekly format
        //         // if (($request->weeklyFormat === 1) && (!empty($request->weekly)) {


        //         // }
        //         // store slots for both weekly and biWeekly formats

        //             foreach ($request->weekly as $slot1) {
        //                 $obj = (object)$slot1;
        //                 // dd($obj);
        //                 if ($data->day === $obj->name) {
        //                     $booking = new Booking;
        //                     $booking->doctor_id = $request->doctorId;
        //                     $booking->name = $request->get('name');
        //                     $booking->date = $data->date;
        //                     $booking->slots = $this->getTimeSlots(20,$obj->start,$obj->end);
        //                     $booking->day = $data->day;
        //                     $booking->weekly_format = 1;
        //                     $booking->save();
        //                 }
        //             }
        //             foreach ($request->biWeekly as $slot2) {
        //                 $obj = (object)($slot2);
        //                 // dd($obj);
        //                 if ($data->day === $obj->name) {
        //                     $booking = new Booking;
        //                     $booking->doctor_id = $request->doctorId;
        //                     $booking->name = $request->get('name');
        //                     $booking->date = $data->date;
        //                     $booking->slots = $this->getTimeSlots(20,$obj->start,$obj->end);
        //                     $booking->day = $data->day;
        //                     $booking->weekly_format = 2;
        //                     $booking->save();
        //                 }
        //             }
        //         // end bi weekly format insert
        //     }
        // }
        else{
            // return message if doctor exists
            return "Soryy doctor already exists";
        }


        return Booking::get(['day','date','slots']);

        // return view('dates', compact('dates'));
    }

    public function doctorSlots(Request $request) {
        return Booking::where('doctor_id',$request->doctorId)->get(['day','date','slots']);
    }

    public function book(Request $request)
    {
        if ($request->get('reoccuring') == 2) {

            // Get the date range from x to y
            // get the dates of each of the reoccuring days
            // save to database as seperate bookings with the new date on that day

            // Get the date range
            $fromDate = new Carbon($request->get('dateTime'));
            $toDate = new Carbon($request->get('reoccurrance_ends_at'));

            $dates = [];

            for ($date = $fromDate; $date->lte($toDate); $date->addDay()) {
                $dates[] = $date->format('l');
            }

            print_r($dates);
            // foreach($dates as $date){
            // if($date->dayOfWeek == Carbon::THURSDAY)
            // {
            //     $today = $date;
            //     echo $today;
            // }

            // for ($date = $fr; $date->dayOfWeek == Carbon::THURSDAY; $date++) {
            //     echo $date;
            // }
            // }
            // return $dates;

        }
        //  else {

        //     $booking = new Booking;
        //     $booking->name = $request->get('name');
        //     $booking->email = $request->get('email');
        //     $booking->phone = $request->get('phone');
        //     $booking->dateTime = $request->get('dateTime');
        //     $booking->reoccuring = $request->get('reoccuring');
        //     $booking->event_type = $request->get('event_type');
        //     $booking->save();

        // }
    }

    public function calendar($date = null)
    {
        $date = empty($date) ? Carbon::now() : Carbon::createFromDate($date);
        $startOfCalendar = $date->copy()->firstOfMonth()->startOfWeek(Carbon::SUNDAY);
        $endOfCalendar = $date->copy()->lastOfMonth()->endOfWeek(Carbon::SATURDAY);

        $html = '';
        while ($startOfCalendar <= $endOfCalendar) {
            $html .= $startOfCalendar->format('j');
            $startOfCalendar->addDay();
        }
        return $html;
    }

    public  function getTimeSlots($duration, $start, $end)
    {
        $time = array();

        $start = new DateTime($start);

        $end = new DateTime($end);

        $start_time = $start->format('H:i');

        $end_time = $end->format('H:i');

        $currentTime = strtotime(Date('Y-m-d H:i'));
        $i=0;

        while(strtotime($start_time) <= strtotime($end_time)){

            $start = $start_time;

            $end = date('H:i',strtotime('+'.$duration.' minutes',strtotime($start_time)));

            $start_time = date('H:i',strtotime('+'.$duration.' minutes',strtotime($start_time)));

            $today = Date('Y-m-d');

            $slotTime = strtotime($today.' '.$start);

            if($slotTime > $currentTime){

                if(strtotime($start_time) <= strtotime($end_time)){
///
                    $time[$i]['start'] = $start;
                    $time[$i]['end'] = $end;
//                $time[$i]=[$start . "-". $end];
//                $time[$i]=$start .' ' . "-". ' ' . $end;
                }
                $i++;
            }

        }
        return json_encode($time);
    }

    public function runSlotsPerDay() {
        $getDate = Booking::where('day','=','Monday')->get();
        // print_r($getDate);

        $slotsData =[];
        $arrayData=[];

        foreach($getDate as $slots) {
            $slots = (object) [
                'day' => $slots->day,
                'date' => $slots->date,
                'slots' => $this->getTimeSlots(20, "16:00","18:55")
            ];
            array_push($slotsData, json_encode($slots));
        }

        // print_r($generateSlots);
    }

    public function allSlots() {
        $getDate = Booking::all();
        // print_r($getDate);

        $slotsData =[];
        $arrayData=[];

        foreach($getDate as $slots) {
            $slots = (object) [
                'day' => $slots->day,
                'date' => $slots->date,
                'slots' => $this->getTimeSlots(20, "14:00","19:55")
            ];
            array_push($slotsData, json_encode($slots,JSON_FORCE_OBJECT));
        }
        print_r($slotsData);

        $generateSlots = $this->getTimeSlots(20,"12:00","16:55");
        // print_r($generateSlots);
    }
}
